from electroncash import version
from electroncash.bitcoin import TYPE_ADDRESS
from electroncash.i18n import _
from electroncash.keystore import Hardware_KeyStore
from electroncash.util import print_error
from electroncash.wallet import Multisig_Wallet

from .versionutil import is_compatible_version

COIN_VALUE_MINUS_MINING_FEE = "!"
ERR_GEN_ADDR = _("Wallet type cannot generate new addresses")
ERR_HARDWARE = _("Hardware wallet")
ERR_MULTISIG = _("Multisig wallet")
ERR_OTHER = _("Unknown wallet")
ERR_SLP = _("SLP wallet")
ERR_VERSION = _("Too old version of Electron Cash")
ERR_WATCHINGONLY = _("Watching-only wallet")


"""
Checks if this wallet is compatible with this plugin
"""
def is_wallet_compatible(wallet):

    is_watching_only = getattr(wallet, 'is_watching_only', lambda: False)
    if (is_watching_only()):
        return False, ERR_WATCHINGONLY

    # We assume that we can create an address to send coins to ourselves.
    # This assumption does not work for all wallets, such as
    # ImportedPrivkeyWallet
    can_create_addr = getattr(wallet, 'create_new_address', None)
    if can_create_addr is None or not callable(can_create_addr):
        return False, ERR_GEN_ADDR

    wallet_type = wallet.storage.get('wallet_type', '')
    if "slp" in wallet_type.lower():
        return False, ERR_SLP

    # Version < 4.0.10 may burn SLP tokens
    if not is_compatible_version(version.PACKAGE_VERSION):
        return False, ERR_VERSION

    try:
        if (isinstance(wallet, (Multisig_Wallet,))):
            return False, ERR_MULTISIG

        keystores = wallet.get_keystores()
        if (any([isinstance(k, Hardware_KeyStore) for k in keystores])):
            return False, ERR_HARDWARE

    except (ImportError, AttributeError) as e:
        # Electron Cash API change? Not sure if safe. Bail.
        print_error("Error checking wallet compatibility:", repr(e))
        return False, ERR_OTHER

    return True, None


def spend_coin_to_self(wallet, password, plugin_config, pledge_coin, label):
    """Spend one coin to self.

    Return: Completed transaction that spent the coin.
    """
    tx = _coin_to_self_tx(wallet, plugin_config, pledge_coin)
    wallet.sign_transaction(tx, password)
    txid = _strict_broadcast(wallet, tx)
    wallet.set_label(txid, label)
    return tx

def get_wallet_address(wallet):
    """
    Use unused address if available. Creating a new address and using that
    when an unused exists creates "unused address gap", confusing software
    importing this wallet with mnemonic seed.
    """
    addr = wallet.get_unused_address(for_change = False, frozen_ok = False)
    if addr is not None:
        return addr

    return wallet.create_new_address(for_change = False)

def _coin_to_self_tx(wallet, plugin_config, coin):
    """Create a new tx that spends one coin to self.

    Return: unsigned transaction
    """
    target_addr = get_wallet_address(wallet)
    return wallet.make_unsigned_transaction(
        inputs=[coin],
        outputs=[(TYPE_ADDRESS, target_addr, COIN_VALUE_MINUS_MINING_FEE)],
        config=plugin_config)


def _strict_broadcast(wallet, signed_tx):
    """Broadcast the signed tx and raise error for anything that goes wrong.

    Return: successful txid
    """
    if not wallet.network:
        raise IOError("Connection error")

    ok, details = wallet.network.broadcast_transaction(signed_tx)
    if not ok:
        raise IOError(str(details))
    return details
